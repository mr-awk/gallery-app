# Ubuntu gallery app translation.
# Copyright 2013 Canonical Ltd.
# This file is distributed under the same license as the gallery-app package.
#
msgid ""
msgstr ""
"Project-Id-Version: gallery-app VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-11-22 21:06+0000\n"
"PO-Revision-Date: 2021-05-15 18:00+0000\n"
"Last-Translator: Ettore Atalan <atalanttore@googlemail.com>\n"
"Language-Team: German <https://translate.ubports.com/projects/ubports/"
"gallery-app/de/>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 3.11.3\n"
"X-Launchpad-Export-Date: 2017-04-05 07:16+0000\n"

#: rc/qml/AlbumEditor/AlbumEditMenu.qml:43
#: rc/qml/AlbumEditor/AlbumEditor.qml:36
msgid "Edit album"
msgstr "Album bearbeiten"

#: rc/qml/AlbumEditor/AlbumEditMenu.qml:50
#: rc/qml/AlbumViewer/AlbumViewer.qml:378 rc/qml/Components/DeleteDialog.qml:32
#: rc/qml/EventsOverview.qml:139 rc/qml/MediaViewer/MediaViewer.qml:350
#: rc/qml/PhotosOverview.qml:195 rc/qml/Utility/EditingHUD.qml:66
msgid "Delete"
msgstr "Löschen"

#: rc/qml/AlbumEditor/AlbumEditor.qml:39 rc/qml/Components/MediaSelector.qml:93
#: rc/qml/EventsOverview.qml:161 rc/qml/MediaViewer/MediaViewer.qml:278
#: rc/qml/PhotosOverview.qml:218 rc/qml/PickerScreen.qml:285
msgid "Cancel"
msgstr "Abbrechen"

#: rc/qml/AlbumViewer/AlbumCoverList.qml:113
msgid "Default"
msgstr "Standard"

#: rc/qml/AlbumViewer/AlbumCoverList.qml:114
msgid "Blue"
msgstr "Blau"

#: rc/qml/AlbumViewer/AlbumCoverList.qml:115
msgid "Green"
msgstr "Grün"

#: rc/qml/AlbumViewer/AlbumCoverList.qml:116
msgid "Pattern"
msgstr "Muster"

#: rc/qml/AlbumViewer/AlbumCoverList.qml:117
msgid "Red"
msgstr "Rot"

#: rc/qml/AlbumViewer/AlbumViewer.qml:107 rc/qml/MainScreen.qml:62
msgid "Album"
msgstr "Album"

#: rc/qml/AlbumViewer/AlbumViewer.qml:372
#: rc/qml/MediaViewer/MediaViewer.qml:340
msgid "Add to album"
msgstr "Zum Album hinzufügen"

#: rc/qml/AlbumsOverview.qml:229 rc/qml/Components/PopupAlbumPicker.qml:91
msgid "Add new album"
msgstr "Neues Album hinzufügen"

#: rc/qml/AlbumsOverview.qml:233 rc/qml/Components/PopupAlbumPicker.qml:101
msgid "New Photo Album"
msgstr "Neues Fotoalbum"

#: rc/qml/AlbumsOverview.qml:234 rc/qml/Components/PopupAlbumPicker.qml:102
msgid "Subtitle"
msgstr "Untertitel"

#: rc/qml/AlbumsOverview.qml:241 rc/qml/EventsOverview.qml:116
#: rc/qml/PhotosOverview.qml:173
msgid "Camera"
msgstr "Kamera"

#: rc/qml/Components/DeleteDialog.qml:36 rc/qml/MediaViewer/MediaViewer.qml:226
msgid "Yes"
msgstr "Ja"

#: rc/qml/Components/DeleteDialog.qml:45 rc/qml/MediaViewer/MediaViewer.qml:237
msgid "No"
msgstr "Nein"

#: rc/qml/Components/DeleteOrDeleteWithContentsDialog.qml:51
msgid "Delete album"
msgstr "Album löschen"

#: rc/qml/Components/DeleteOrDeleteWithContentsDialog.qml:60
msgid "Delete album AND contents"
msgstr "Album UND Inhalte löschen"

#: rc/qml/Components/EventCard.qml:59
msgid "MMM yyyy"
msgstr "MMM yyyy"

#: rc/qml/Components/EventCard.qml:73
msgid "dd"
msgstr "dd"

#: rc/qml/Components/MediaSelector.qml:59
#: rc/qml/Components/MediaSelector.qml:81
msgid "Add to Album"
msgstr "Zum Album hinzufügen"

#: rc/qml/Components/PopupAlbumPicker.qml:162 rc/qml/Utility/EditingHUD.qml:77
msgid "Add Photo to Album"
msgstr "Foto zum Album hinzufügen"

#: rc/qml/Components/UnableShareDialog.qml:25
msgid "Unable to share"
msgstr "Teilen nicht möglich"

#: rc/qml/Components/UnableShareDialog.qml:26
msgid "Unable to share photos and videos at the same time"
msgstr "Kann Fotos und Videos nicht gleichzeitig teilen"

#: rc/qml/Components/UnableShareDialog.qml:30
msgid "Ok"
msgstr "OK"

#: rc/qml/EventsOverview.qml:80 rc/qml/PhotosOverview.qml:87
#, qt-format
msgid "Delete %1 photo"
msgid_plural "Delete %1 photos"
msgstr[0] "%1 Foto löschen"
msgstr[1] "%1 Fotos löschen"

#: rc/qml/EventsOverview.qml:86 rc/qml/PhotosOverview.qml:93
#, qt-format
msgid "Delete %1 video"
msgid_plural "Delete %1 videos"
msgstr[0] "%1 Video löschen"
msgstr[1] "%1 Videos löschen"

#: rc/qml/EventsOverview.qml:92 rc/qml/PhotosOverview.qml:99
#, qt-format
msgid "Delete %1 media file"
msgid_plural "Delete %1 media files"
msgstr[0] "%1 Mediendatei löschen"
msgstr[1] "%1 Mediendateien löschen"

#: rc/qml/EventsOverview.qml:109 rc/qml/MainScreen.qml:199
#: rc/qml/MainScreen.qml:251 rc/qml/PhotosOverview.qml:166
#: rc/qml/PickerScreen.qml:192 rc/qml/PickerScreen.qml:239
msgid "Select"
msgstr "Auswählen"

#: rc/qml/EventsOverview.qml:128 rc/qml/PhotosOverview.qml:185
#: rc/qml/Utility/EditingHUD.qml:76
msgid "Add"
msgstr "Hinzufügen"

#: rc/qml/EventsOverview.qml:146 rc/qml/MediaViewer/MediaViewer.qml:361
#: rc/qml/PhotosOverview.qml:202 rc/qml/Utility/EditingHUD.qml:71
msgid "Share"
msgstr "Teilen"

#: rc/qml/EventsOverview.qml:195 rc/qml/MediaViewer/MediaViewer.qml:182
#: rc/qml/PhotosOverview.qml:241
msgid "Share to"
msgstr "Teilen über"

#: rc/qml/LoadingScreen.qml:39
msgid "Loading…"
msgstr "Wird geladen…"

#: rc/qml/MainScreen.qml:148
msgid "Albums"
msgstr "Alben"

#: rc/qml/MainScreen.qml:162 rc/qml/MainScreen.qml:201
#: rc/qml/PickerScreen.qml:154
msgid "Events"
msgstr "Ereignisse"

#: rc/qml/MainScreen.qml:210 rc/qml/MainScreen.qml:253
#: rc/qml/PickerScreen.qml:203
msgid "Photos"
msgstr "Fotos"

#: rc/qml/MediaViewer/MediaViewer.qml:215
msgid "Delete a photo"
msgstr "Ein Foto löschen"

#: rc/qml/MediaViewer/MediaViewer.qml:215
msgid "Delete a video"
msgstr "Ein Video löschen"

#: rc/qml/MediaViewer/MediaViewer.qml:248
msgid "Remove a photo from album"
msgstr "Ein Foto aus dem Album entfernen"

#: rc/qml/MediaViewer/MediaViewer.qml:248
msgid "Remove a video from album"
msgstr "Ein Video aus dem Album entfernen"

#: rc/qml/MediaViewer/MediaViewer.qml:257
msgid "Remove from Album"
msgstr "Aus Album entfernen"

#: rc/qml/MediaViewer/MediaViewer.qml:268
msgid "Remove from Album and Delete"
msgstr "Aus Album entfernen und löschen"

#: rc/qml/MediaViewer/MediaViewer.qml:315
msgid "Edit"
msgstr "Bearbeiten"

#: rc/qml/MediaViewer/MediaViewer.qml:371
msgid "Info"
msgstr "Info"

#: rc/qml/MediaViewer/MediaViewer.qml:394
msgid "Informations"
msgstr "Informationen"

#: rc/qml/MediaViewer/MediaViewer.qml:397
msgid "Media type: "
msgstr "Medientyp: "

#: rc/qml/MediaViewer/MediaViewer.qml:398
msgid "photo"
msgstr "Foto"

#: rc/qml/MediaViewer/MediaViewer.qml:398
msgid "video"
msgstr "Video"

#: rc/qml/MediaViewer/MediaViewer.qml:399
msgid "Media name: "
msgstr "Medienname "

#: rc/qml/MediaViewer/MediaViewer.qml:400
msgid "Date: "
msgstr "Datum: "

#: rc/qml/MediaViewer/MediaViewer.qml:401
msgid "Time: "
msgstr "Uhrzeit: "

#: rc/qml/MediaViewer/MediaViewer.qml:407
msgid "ok"
msgstr "OK"

#: rc/qml/MediaViewer/PhotoEditorPage.qml:26
msgid "Edit Photo"
msgstr "Foto bearbeiten"

#: rc/qml/MediaViewer/PopupPhotoViewer.qml:46
#: desktop/gallery-app.desktop.in.in.h:1
msgid "Gallery"
msgstr "Galerie"

#: rc/qml/MediaViewer/PopupPhotoViewer.qml:183
msgid "Toggle Selection"
msgstr "Markierung umschalten"

#: rc/qml/MediaViewer/SingleMediaViewer.qml:241
msgid "An error has occurred attempting to load media"
msgstr "Fehler beim Laden der Mediendatei"

#: rc/qml/PhotosOverview.qml:116 rc/qml/PhotosOverview.qml:159
msgid "Grid Size"
msgstr "Rastergröße"

#: rc/qml/PhotosOverview.qml:117
msgid "Select the grid size in gu units between 8 and 20 (default is 12)"
msgstr ""
"Wählen Sie die Rastergröße in gu-Einheiten zwischen 8 und 20 (Standard ist "
"12)"

#: rc/qml/PhotosOverview.qml:132
msgid "Finished"
msgstr "Erledigt"

#: rc/qml/PickerScreen.qml:291
msgid "Pick"
msgstr "Auswählen"

#: rc/qml/Utility/EditingHUD.qml:67
msgid "Trash;Erase"
msgstr "Papierkorb;Löschen"

#: rc/qml/Utility/EditingHUD.qml:72
msgid "Post;Upload;Attach"
msgstr "Posten;Hochladen;Anhängen"

#: rc/qml/Utility/EditingHUD.qml:81
msgid "Undo"
msgstr "Rückgängig"

#: rc/qml/Utility/EditingHUD.qml:82
msgid "Cancel Action;Backstep"
msgstr "Aktion abbrechen;Rückgängig"

#: rc/qml/Utility/EditingHUD.qml:86
msgid "Redo"
msgstr "Wiederholen"

#: rc/qml/Utility/EditingHUD.qml:87
msgid "Reapply;Make Again"
msgstr "Erneut anwenden;Wiederholen"

#: rc/qml/Utility/EditingHUD.qml:91
msgid "Auto Enhance"
msgstr "Automatisch verbessern"

#: rc/qml/Utility/EditingHUD.qml:92
msgid "Adjust the image automatically"
msgstr "Bild automatisch optimieren"

#: rc/qml/Utility/EditingHUD.qml:93
msgid "Automatically Adjust Photo"
msgstr "Foto automatisch ausrichten"

#: rc/qml/Utility/EditingHUD.qml:98
msgid "Rotate"
msgstr "Drehen"

#: rc/qml/Utility/EditingHUD.qml:99
msgid "Turn Clockwise"
msgstr "Im Uhrzeigersinn drehen"

#: rc/qml/Utility/EditingHUD.qml:100
msgid "Rotate the image clockwise"
msgstr "Bild im Uhrzeigersinn drehen"

#: rc/qml/Utility/EditingHUD.qml:105
msgid "Crop"
msgstr "Zuschneiden"

#: rc/qml/Utility/EditingHUD.qml:106
msgid "Trim;Cut"
msgstr "Zuschneiden;Ausschneiden"

#: rc/qml/Utility/EditingHUD.qml:107
msgid "Crop the image"
msgstr "Bild zuschneiden"

#: rc/qml/Utility/EditingHUD.qml:112
msgid "Revert to Original"
msgstr "Original wiederherstellen"

#: rc/qml/Utility/EditingHUD.qml:113
msgid "Discard Changes"
msgstr "Änderungen verwerfen"

#: rc/qml/Utility/EditingHUD.qml:114
msgid "Discard all changes"
msgstr "Alle Änderungen verwerfen"

#: rc/qml/Utility/EditingHUD.qml:118
msgid "Exposure"
msgstr "Belichtung"

#: rc/qml/Utility/EditingHUD.qml:119
msgid "Adjust the exposure"
msgstr "Belichtung anpassen"

#: rc/qml/Utility/EditingHUD.qml:120
msgid "Underexposed;Overexposed"
msgstr "Unterbelichtet;Überbelichtet"

#: rc/qml/Utility/EditingHUD.qml:121 rc/qml/Utility/EditingHUD.qml:162
msgid "Confirm"
msgstr "Bestätigen"

#: rc/qml/Utility/EditingHUD.qml:125
msgid "Compensation"
msgstr "Farbabgleich"

#: rc/qml/Utility/EditingHUD.qml:159
msgid "Color Balance"
msgstr "Farbbalance"

#: rc/qml/Utility/EditingHUD.qml:160
msgid "Adjust color balance"
msgstr "Farbbalance anpassen"

#: rc/qml/Utility/EditingHUD.qml:161
msgid "Saturation;Hue"
msgstr "Sättigung;Farbton"

#: rc/qml/Utility/EditingHUD.qml:166
msgid "Brightness"
msgstr "Helligkeit"

#: rc/qml/Utility/EditingHUD.qml:177
msgid "Contrast"
msgstr "Kontrast"

#: rc/qml/Utility/EditingHUD.qml:188
msgid "Saturation"
msgstr "Sättigung"

#: rc/qml/Utility/EditingHUD.qml:199
msgid "Hue"
msgstr "Farbton"

#: desktop/gallery-app.desktop.in.in.h:2
msgid "Ubuntu Photo Viewer"
msgstr "Ubuntu-Fotobetrachter"

#: desktop/gallery-app.desktop.in.in.h:3
msgid "Browse your photographs"
msgstr "Ihre Fotos durchsuchen"

#: desktop/gallery-app.desktop.in.in.h:4
msgid "Photographs;Pictures;Albums"
msgstr "Fotos;Bilder;Alben"

#~ msgid "Delete 1 photo"
#~ msgstr "1 Foto löschen"

#~ msgid "Delete 1 video"
#~ msgstr "1 Video löschen"

#~ msgid "Delete %1 photos and 1 video"
#~ msgstr "%1 Fotos und 1 Video löschen"

#~ msgid "Delete 1 photo and %1 videos"
#~ msgstr "1 Foto und %1 Videos löschen"

#~ msgid "Delete 1 photo and 1 video"
#~ msgstr "1 Foto und 1 Video löschen"

#~ msgid "Delete %1 photos and %2 videos"
#~ msgstr "%1 Fotos und %2 Videos löschen"
